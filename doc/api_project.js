define({
  "name": "Datspot",
  "version": "1.0.1",
  "description": "API documentation for Datspot",
  "apidoc": "0.3.0",
  "engines": {
    "node": ">=8.11.1"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-08-24T13:33:44.650Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
