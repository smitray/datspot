define({ "api": [
  {
    "type": "put",
    "url": "/api/auth/",
    "title": "Update auth module",
    "name": "Get_other",
    "group": "Authentication",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "acc_type",
            "description": "<p>User's account type.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"email\": \"john@doe.com\",\n  \"password\": \"test123456\",\n  \"acc_type\": \"user\" // artist, club or promoter\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"auth\": [Object]\n  },\n  \"message\": \"auth updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/local",
    "title": "Local Signup and Login",
    "name": "Local_Authentication",
    "group": "Authentication",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User's full name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "accType",
            "defaultValue": "user",
            "description": "<p>User's account type</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "signp",
            "defaultValue": "false",
            "description": "<p>To toggle between signup and login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Signup",
          "content": "{\n  \"name\": \"John Doe\",\n  \"username\": \"johndoe\",\n  \"password\": \"test1234\",\n  \"email\": \"john@doe.com\",\n  \"accType\": \"admin\",\n  \"signup\": true\n}",
          "type": "json"
        },
        {
          "title": "Login",
          "content": "{\n  \"username\": \"johndoe\", //Username or email as value but key should be \"username\"\n  \"password\": \"test1234\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"token\": \"Bearer JWT token\"\n  },\n  \"message\": \"Loggedin successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 409 Record conflict",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/social",
    "title": "Social Signup and Login",
    "name": "Social_Authentication",
    "group": "Authentication",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User's full name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "scId",
            "description": "<p>Social network ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "scToken",
            "description": "<p>Social network token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "scType",
            "description": "<p>Name of the social network</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"name\": \"John Doe\",\n  \"username\": \"johndoe\",\n  \"email\": \"john@doe.com\",\n  \"scId\": \"123456789\",\n  \"scToken\": \"123456789\",\n  \"scType\": \"facebook\" // Or twitter / google\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"token\": \"Bearer JWT token\"\n  },\n  \"message\": \"Loggedin successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 409 Record conflict",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/chat",
    "title": "Create chat",
    "name": "Create_chat",
    "group": "Chat",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    chat: [Object]\n  },\n  message: chat created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/chat/controller.js",
    "groupTitle": "Chat"
  },
  {
    "type": "post",
    "url": "/api/chat/:id",
    "title": "Delete chat",
    "name": "Delete_chat",
    "group": "Chat",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    chat: [Object]\n  },\n  message: chat deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/chat/controller.js",
    "groupTitle": "Chat"
  },
  {
    "type": "get",
    "url": "/api/chat",
    "title": "Get all chat",
    "name": "Get_all_chat",
    "group": "Chat",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    chats: [Object]\n  },\n  message: chat details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/chat/controller.js",
    "groupTitle": "Chat"
  },
  {
    "type": "get",
    "url": "/api/chat/:id",
    "title": "Get single chat",
    "name": "Get_single_chat",
    "group": "Chat",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    chat: [Object]\n  },\n  message: chat details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/chat/controller.js",
    "groupTitle": "Chat"
  },
  {
    "type": "post",
    "url": "/api/chat/:id",
    "title": "Update chat",
    "name": "Update_chat",
    "group": "Chat",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    chat: [Object]\n  },\n  message: chat updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/chat/controller.js",
    "groupTitle": "Chat"
  },
  {
    "type": "get",
    "url": "/api/city",
    "title": "Get all citys",
    "name": "Get_citys",
    "group": "City",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"citys\": [Object]\n  },\n  \"message\": \"citys fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/city/controller.js",
    "groupTitle": "City"
  },
  {
    "type": "post",
    "url": "/api/comments",
    "title": "Create comments",
    "name": "Create_comments",
    "group": "Comments",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "commentId",
            "description": "<p>comment id if exists</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>comment</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"commentId\": [ObjectID],\n  \"content\": \"Lorem ipsum\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    comments: [Object]\n  },\n  message: comments created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/comments/controller.js",
    "groupTitle": "Comments"
  },
  {
    "type": "delete",
    "url": "/api/comments/:id",
    "title": "Delete comments",
    "name": "Delete_comments",
    "group": "Comments",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    comments: [Object]\n  },\n  message: comments deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/comments/controller.js",
    "groupTitle": "Comments"
  },
  {
    "type": "get",
    "url": "/api/comments",
    "title": "Get all comments",
    "name": "Get_all_comments",
    "group": "Comments",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    commentss: [Object]\n  },\n  message: comments details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/comments/controller.js",
    "groupTitle": "Comments"
  },
  {
    "type": "put",
    "url": "/api/comments/:id",
    "title": "Update comments",
    "name": "Update_comments",
    "group": "Comments",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    comments: [Object]\n  },\n  message: comments updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/comments/controller.js",
    "groupTitle": "Comments"
  },
  {
    "type": "post",
    "url": "/api/datspot",
    "title": "Create datspot",
    "name": "Create_datspot",
    "group": "Datspot",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    datspot: [Object]\n  },\n  message: datspot created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/datspot/controller.js",
    "groupTitle": "Datspot"
  },
  {
    "type": "delete",
    "url": "/api/datspot/:id",
    "title": "Delete datspot",
    "name": "Delete_datspot",
    "group": "Datspot",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    datspot: [Object]\n  },\n  message: datspot deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/datspot/controller.js",
    "groupTitle": "Datspot"
  },
  {
    "type": "get",
    "url": "/api/datspot",
    "title": "Get all datspot",
    "name": "Get_all_datspot",
    "group": "Datspot",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    datspots: [Object]\n  },\n  message: datspot details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/datspot/controller.js",
    "groupTitle": "Datspot"
  },
  {
    "type": "get",
    "url": "/api/datspot/:type",
    "title": "Get single datspot",
    "name": "Get_single_datspot",
    "group": "Datspot",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    datspot: [Object]\n  },\n  message: datspot details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/datspot/controller.js",
    "groupTitle": "Datspot"
  },
  {
    "type": "put",
    "url": "/api/datspot/:id",
    "title": "Update datspot",
    "name": "Update_datspot",
    "group": "Datspot",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    datspot: [Object]\n  },\n  message: datspot updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/datspot/controller.js",
    "groupTitle": "Datspot"
  },
  {
    "type": "put",
    "url": "/api/gallery/comment/:id",
    "title": "Add comment to gallery",
    "name": "Add_comment_to_gallery",
    "group": "Gallery",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectID",
            "optional": false,
            "field": "comments",
            "description": "<p>comment id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"comments\": [Object ID]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    gallery: [Object]\n  },\n  message: gallery updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/gallery/controller.js",
    "groupTitle": "Gallery"
  },
  {
    "type": "put",
    "url": "/api/gallery/like/:id",
    "title": "Add like to gallery",
    "name": "Add_like_to_gallery",
    "group": "Gallery",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    gallery: [Object]\n  },\n  message: gallery updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/gallery/controller.js",
    "groupTitle": "Gallery"
  },
  {
    "type": "post",
    "url": "/api/gallery",
    "title": "Create gallery",
    "name": "Create_gallery",
    "group": "Gallery",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "galleryId",
            "description": "<p>Gallery ID if exists</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>images Object from File api</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"images\": [Object],\n  \"galleryId\": [ObjectId]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    gallery: [Object]\n  },\n  message: gallery created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/gallery/controller.js",
    "groupTitle": "Gallery"
  },
  {
    "type": "delete",
    "url": "/api/gallery/:id",
    "title": "Delete gallery",
    "name": "Delete_gallery",
    "group": "Gallery",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    gallery: [Object]\n  },\n  message: gallery deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/gallery/controller.js",
    "groupTitle": "Gallery"
  },
  {
    "type": "get",
    "url": "/api/gallery",
    "title": "Get all gallery",
    "name": "Get_all_gallery",
    "group": "Gallery",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    gallerys: [Object]\n  },\n  message: gallery details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/gallery/controller.js",
    "groupTitle": "Gallery"
  },
  {
    "type": "get",
    "url": "/api/gallery/item/:id",
    "title": "Get gallery single item",
    "name": "Get_gallery_single_item",
    "group": "Gallery",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    gallery: [Object]\n  },\n  message: gallery details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/gallery/controller.js",
    "groupTitle": "Gallery"
  },
  {
    "type": "get",
    "url": "/api/gallery/:id",
    "title": "Get single gallery",
    "name": "Get_single_gallery",
    "group": "Gallery",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    gallery: [Object]\n  },\n  message: gallery details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/gallery/controller.js",
    "groupTitle": "Gallery"
  },
  {
    "type": "put",
    "url": "/api/gallery/dislike/:id",
    "title": "Remove like to gallery",
    "name": "Remove_like_to_gallery",
    "group": "Gallery",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    gallery: [Object]\n  },\n  message: gallery updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/gallery/controller.js",
    "groupTitle": "Gallery"
  },
  {
    "type": "post",
    "url": "/api/music",
    "title": "Create music record",
    "name": "Create_music",
    "group": "Music",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "genre",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "genValue",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"genre\": \"Jazz\",\n  \"genValue\": \"jazz\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"music\": [Object]\n  },\n  \"message\": \"music created successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/music/controller.js",
    "groupTitle": "Music"
  },
  {
    "type": "get",
    "url": "/api/music/:id",
    "title": "Get single music",
    "name": "Get_music",
    "group": "Music",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"music\": [Object]\n  },\n  \"message\": \"music fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/music/controller.js",
    "groupTitle": "Music"
  },
  {
    "type": "get",
    "url": "/api/music",
    "title": "Get all musics",
    "name": "Get_musics",
    "group": "Music",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"musics\": [Object]\n  },\n  \"message\": \"musics fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/music/controller.js",
    "groupTitle": "Music"
  },
  {
    "type": "post",
    "url": "/api/posts",
    "title": "Create posts",
    "name": "Create_posts",
    "group": "Posts",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    posts: [Object]\n  },\n  message: posts created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/posts/controller.js",
    "groupTitle": "Posts"
  },
  {
    "type": "delete",
    "url": "/api/posts/:id",
    "title": "Delete posts",
    "name": "Delete_posts",
    "group": "Posts",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    posts: [Object]\n  },\n  message: posts deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/posts/controller.js",
    "groupTitle": "Posts"
  },
  {
    "type": "get",
    "url": "/api/posts",
    "title": "Get all posts",
    "name": "Get_all_posts",
    "group": "Posts",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    postss: [Object]\n  },\n  message: posts details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/posts/controller.js",
    "groupTitle": "Posts"
  },
  {
    "type": "get",
    "url": "/api/posts/:id",
    "title": "Get single posts",
    "name": "Get_single_posts",
    "group": "Posts",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    posts: [Object]\n  },\n  message: posts details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/posts/controller.js",
    "groupTitle": "Posts"
  },
  {
    "type": "put",
    "url": "/api/posts/:id",
    "title": "Update posts",
    "name": "Update_posts",
    "group": "Posts",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    posts: [Object]\n  },\n  message: posts updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/posts/controller.js",
    "groupTitle": "Posts"
  },
  {
    "type": "post",
    "url": "/api/user/request/accept",
    "title": "Accept request",
    "name": "Accept_friend_request",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "fid",
            "description": "<p>friend's ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"fid\": [ObjectID]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/user/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/user/request",
    "title": "Create request",
    "name": "Create_friend_request",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "fid",
            "description": "<p>friend's ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"fid\": [ObjectID]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/user/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/user/friends",
    "title": "Get friends list",
    "name": "Get_friends_list",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"users\": [Object]\n  },\n  \"message\": \"all friends fetche\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/user/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/user/request",
    "title": "Get friends request",
    "name": "Get_friends_request_list",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"users\": [Object]\n  },\n  \"message\": \"all requests fetche\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/user/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/auth",
    "title": "Get my details",
    "name": "Get_me",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"user\": [Object]\n  },\n  \"message\": \"User details fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/auth/:id",
    "title": "Get other's details",
    "name": "Get_other",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"user\": [Object]\n  },\n  \"message\": \"User details fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/user/friends",
    "title": "Delete friend",
    "name": "Remove_friend",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "fid",
            "description": "<p>friend's ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"fid\": [ObjectID]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/user/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/user/request/reject",
    "title": "Remove request",
    "name": "Remove_friend_request",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "fid",
            "description": "<p>friend's ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"fid\": [ObjectID]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/user/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/api/user",
    "title": "Update user details",
    "name": "Update_user",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "full_name",
            "description": "<p>User's full name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dp",
            "description": "<p>File _id that is received from File upload api</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "music",
            "description": "<p>User's music choice</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relationship",
            "description": "<p>User's relationship status</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>User's city</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "about",
            "description": "<p>User's bio</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "dob",
            "description": "<p>User's date of birth [Format YYYY-MM-DD]</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"full_name\": \"Smit Ray\",\n  \"dp\": \"File _id\",\n  \"music\": [\n    \"trance\",\n    \"pop\",\n    \"rock\"\n  ],\n  \"relationship\": \"single\",\n  \"city\": \"london\",\n  \"about\": \"something about me\",\n  \"dob\": \"1987-02-16\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"user\": [Object]\n  },\n  \"message\": \"user updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/user/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/files/delete",
    "title": "File delete",
    "name": "Delete_file",
    "group": "Utility",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fileId",
            "description": "<p>File / Files (Use form data)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"fileId\": \"file\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"files\": {\n      \"filename\": \"file name\",\n      \"permalink\": \"link of the file\",\n      \"_id\": \"file ID\"\n    }\n  },\n  \"message\": \"File deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/files/controller.js",
    "groupTitle": "Utility"
  },
  {
    "type": "post",
    "url": "/api/files",
    "title": "File upload",
    "name": "Upload_file",
    "group": "Utility",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "docs",
            "description": "<p>File / Files (Use form data)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"docs\": \"file\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"files\": {\n      \"filename\": \"file name\",\n      \"permalink\": \"link of the file\",\n      \"_id\": \"file ID\"\n    }\n  },\n  \"message\": \"All files uploaded\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/files/controller.js",
    "groupTitle": "Utility"
  }
] });
