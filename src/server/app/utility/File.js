import { generate } from 'shortid';
import { join, extname } from 'path';
import fs from 'fs-extra';
import config from 'config';
import ffmpeg from 'fluent-ffmpeg';
import sharp from 'sharp';

const fileUpload = async (file) => {
  const ext = extname(file.name);
  const fileType = file.type.split('/')[0];
  const newFilename = `${Date.now()}-${generate()}${ext}`;
  const filePath = join(config.get('paths.static'), newFilename);
  let thumbFile;
  try {
    await fs.copy(file.path, filePath);
    if (fileType === 'image') {
      thumbFile = newFilename;
      await sharp(filePath)
        .resize(200, 200)
        .png()
        .toFile(join(config.get('paths.static'), `thumbs/${thumbFile}`));
    } else if (fileType === 'application') {
      thumbFile = 'generic/default-file.png';
    } else if (fileType === 'video') {
      thumbFile = newFilename;
      await ffmpeg(filePath)
        .screenshots({
          timestamps: ['5%'],
          filename: thumbFile,
          folder: join(config.get('paths.static'), 'thumbs/'),
          size: '320x240'
        });
    }
  } catch (e) {
    console.log(e); //eslint-disable-line
  }
  return {
    file: newFilename,
    thumbnail: thumbFile
  };
};

const fileDelete = async (file) => {
  const filePath = join(config.get('paths.static'), file);
  try {
    await fs.remove(filePath);
  } catch (e) {
    console.log(e); //eslint-disable-line
  }
  return file;
};

export {
  fileUpload,
  fileDelete
};
