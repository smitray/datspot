import nmailer from 'nodemailer';
// import config from 'config';


const smtpTransport = nmailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'smitray.test@gmail.com',
    pass: 'test@12345'
  }
});

const sendEmail = async ({ body, to }) => {
  const mailOpt = {
    from: 'Smit Ray <humancoder@outlook.com>',
    to,
    subject: 'no-eply | Datspot',
    text: body
  };

  try {
    await smtpTransport.sendMail(mailOpt);
  } catch (e) {
    throw new Error(e.message);
  }
};

export default sendEmail;
