import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const datspotSchema = new mongoose.Schema({
  ctType: String,
  location: String,
  ctId: mongoose.Schema.Types.ObjectId
});

datspotSchema.plugin(uniqueValidator);
datspotSchema.plugin(timestamp);

const datspotModel = mongoose.model('datspotModel', datspotSchema);

export default datspotModel;
