export * as datspotRouteProps from './router';
export {
  datspotAll,
  datspotSingle,
  datspotCreate,
  datspotUpdate,
  datspotDelete
} from './controller';
export { default as datspotModel } from './datspot.model';
export { default as datspotCrud } from './datspot.service';

