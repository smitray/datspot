import { Crud } from '@utl';

import datspotModel from './datspot.model';

class Servicedatspot extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const datspotCrud = new Servicedatspot(datspotModel);
export default datspotCrud;
