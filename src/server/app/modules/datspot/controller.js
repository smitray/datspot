import { datspotCrud } from './';


let datspots;
let datspot;
let datspotNew;

/**
@api {get} /api/datspot Get all datspot
@apiName Get all datspot
@apiGroup Datspot
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    datspots: [Object]
  },
  message: datspot details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const datspotAll = async (ctx) => {
  try {
    datspots = await datspotCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        datspots
      },
      message: 'datspots fetched successfully'
    };
  }
};


/**
@api {get} /api/datspot/:type Get single datspot
@apiName Get single datspot
@apiGroup Datspot
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    datspot: [Object]
  },
  message: datspot details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const datspotSingle = async (ctx) => {
  try {
    datspot = await datspotCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        datspot
      },
      message: 'datspot fetched successfully'
    };
  }
};

/**
@api {post} /api/datspot Create datspot
@apiName Create datspot
@apiGroup Datspot
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    datspot: [Object]
  },
  message: datspot created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const datspotCreate = async (ctx) => {
  try {
    datspotNew = await datspotCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        datspot: datspotNew
      },
      message: 'datspot created successfully'
    };
  }
};

/**
@api {put} /api/datspot/:id Update datspot
@apiName Update datspot
@apiGroup Datspot
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    datspot: [Object]
  },
  message: datspot updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const datspotUpdate = async (ctx) => {
  try {
    datspot = await datspotCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        datspot
      },
      message: 'datspot updated successfully'
    };
  }
};

/**
@api {delete} /api/datspot/:id Delete datspot
@apiName Delete datspot
@apiGroup Datspot
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    datspot: [Object]
  },
  message: datspot deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const datspotDelete = async (ctx) => {
  try {
    datspot = await datspotCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        datspot
      },
      message: 'datspot deleted successfully'
    };
  }
};

export {
  datspotAll,
  datspotSingle,
  datspotCreate,
  datspotUpdate,
  datspotDelete
};
