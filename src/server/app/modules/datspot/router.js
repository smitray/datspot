// import { isAuthenticated } from '@mid';

import {
  datspotAll,
  datspotSingle,
  datspotCreate,
  datspotUpdate,
  datspotDelete
} from './';

export const baseUrl = '/api/datspot';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      datspotAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      datspotSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      datspotUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      datspotDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      datspotCreate
    ]
  }
];
