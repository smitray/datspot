import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const userSchema = new mongoose.Schema({
  full_name: {
    type: String,
    default: null
  },
  dp: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'filesModel',
    default: null
  },
  music: [{
    type: String,
    default: null
  }],
  relationship: {
    type: String,
    default: null
  },
  city: String,
  about: String,
  dob: Date,
  permission: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'permissionModel',
    default: null
  },
  gallery: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'galleryModel',
    default: null
  }
});

userSchema.plugin(uniqueValidator);
userSchema.plugin(timestamp);

const userModel = mongoose.model('userModel', userSchema);

export default userModel;
