import { isAuthenticated } from '@mid';
import {
  userUpdate,
  getFriends,
  deleteFriends,
  acceptRequest,
  rejectRequest,
  getRequests,
  createRequest
} from './';

export const baseUrl = '/api/user';

export const routes = [
  {
    method: 'PUT',
    route: '/',
    handlers: [
      isAuthenticated,
      userUpdate
    ]
  },
  {
    method: 'GET',
    route: '/friends',
    handlers: [
      isAuthenticated,
      getFriends
    ]
  },
  {
    method: 'POST',
    route: '/friends',
    handlers: [
      isAuthenticated,
      deleteFriends
    ]
  },
  {
    method: 'POST',
    route: '/request/accept',
    handlers: [
      isAuthenticated,
      acceptRequest
    ]
  },
  {
    method: 'POST',
    route: '/request/reject',
    handlers: [
      isAuthenticated,
      rejectRequest
    ]
  },
  {
    method: 'GET',
    route: '/request',
    handlers: [
      isAuthenticated,
      getRequests
    ]
  },
  {
    method: 'POST',
    route: '/request',
    handlers: [
      isAuthenticated,
      createRequest
    ]
  }
];
