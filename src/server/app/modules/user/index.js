export * as userRouteProps from './router';
export {
  userUpdate,
  getFriends,
  deleteFriends,
  acceptRequest,
  rejectRequest,
  getRequests,
  createRequest
} from './controller';
export { default as userModel } from './user.model';
export { default as userCrud } from './user.service';
export {
  addressModel,
  addressCrud
} from './address.model';
export {
  permissionCrud,
  permissionModel
} from './permission.model';
export { default as userIO } from './user.io';
