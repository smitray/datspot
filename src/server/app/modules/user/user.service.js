import _ from 'lodash';
import { Crud } from '@utl';

import userModel from './user.model';
import { filesCrud } from '../files';
import { authCrud } from '../auth';
import { permissionCrud } from './permission.model';

class UserService extends Crud {
  constructor(model) {
    super(model);
    this.filesCrud = filesCrud;
    this.authCrud = authCrud;
    this.permissionCrud = permissionCrud;
  }

  async updateUser(options) {
    const {
      full_name, //eslint-disable-line
      dp,
      music,
      relationship,
      city,
      about,
      dob,
      gallery
    } = options.body;
    const auth = await this.authCrud.single(options.params);
    const user = await this.single({
      qr: {
        _id: auth.user
      }
    });

    if (music) {
      const xyz = _.difference(music, user.music);
      xyz.forEach((md) => {
        user.music.push(md);
      });
    }

    if (full_name) {  //eslint-disable-line
      user.full_name = full_name; //eslint-disable-line
    }

    if (dp) {
      user.dp = dp;
    }

    if (relationship) {
      user.relationship = relationship;
    }

    if (city) {
      user.city = city;
    }

    if (about) {
      user.about = about;
    }

    if (dob) {
      user.dob = dob;
    }

    if (gallery) {
      user.gallery = gallery;
    }

    return new Promise((resolve, reject) => {
      user.save().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async createRequest(options) {
    const {
      fid,
      auth
    } = options;
    const authModel = await this.authCrud.single({
      qr: {
        _id: auth
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel'
      }]
    });

    const permission = await this.permissionCrud.single({
      qr: {
        _id: authModel.user.permission
      }
    });
    permission.requestSent.push(fid);
    await permission.save();

    const friend = await this.authCrud.single({
      qr: {
        _id: fid
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel'
      }]
    });

    const frPermission = await this.permissionCrud.single({
      qr: {
        _id: friend.user.permission
      }
    });

    frPermission.request.push(auth);

    return new Promise((resolve, reject) => {
      frPermission.save().then(() => {
        resolve(friend);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async deleteFriend(options) {
    const {
      fid,
      auth
    } = options;
    const authModel = await this.authCrud.single({
      qr: {
        _id: auth
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel'
      }]
    });

    const permission = await this.permissionCrud.single({
      qr: {
        _id: authModel.user.permission
      }
    });

    permission.friends.remove(fid);
    await permission.save();

    const friend = await this.authCrud.single({
      qr: {
        _id: fid
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel'
      }]
    });

    const frPermission = await this.permissionCrud.single({
      qr: {
        _id: friend.user.permission
      }
    });

    frPermission.friends.remove(auth);

    return new Promise((resolve, reject) => {
      frPermission.save().then(() => {
        resolve(friend);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async rejectRequest(options) {
    const {
      fid,
      auth
    } = options;
    const authModel = await this.authCrud.single({
      qr: {
        _id: auth
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel'
      }]
    });

    const permission = await this.permissionCrud.single({
      qr: {
        _id: authModel.user.permission
      }
    });

    permission.request.remove(fid);
    await permission.save();

    const friend = await this.authCrud.single({
      qr: {
        _id: fid
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel'
      }]
    });

    const frPermission = await this.permissionCrud.single({
      qr: {
        _id: friend.user.permission
      }
    });

    frPermission.requestSent.remove(auth);

    return new Promise((resolve, reject) => {
      frPermission.save().then(() => {
        resolve(friend);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async acceptRequest(options) {
    const {
      fid,
      auth
    } = options;
    const authModel = await this.authCrud.single({
      qr: {
        _id: auth
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel'
      }]
    });

    const permission = await this.permissionCrud.single({
      qr: {
        _id: authModel.user.permission
      }
    });

    permission.request.remove(fid);
    permission.friends.push(fid);
    await permission.save();

    const friend = await this.authCrud.single({
      qr: {
        _id: fid
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel',
        select: 'full_name dp music relationship city about permission',
        populate: [{
          path: 'dp',
          model: 'filesModel'
        }]
      }]
    });

    const frPermission = await this.permissionCrud.single({
      qr: {
        _id: friend.user.permission
      }
    });

    frPermission.requestSent.remove(auth);
    frPermission.friends.push(auth);

    return new Promise((resolve, reject) => {
      frPermission.save().then(() => {
        resolve(friend);
      }).catch((e) => {
        reject(e);
      });
    });
  }
}

const userCrud = new UserService(userModel);
export default userCrud;
