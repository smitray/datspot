import { userCrud } from './';
import { authCrud } from '../auth';


let user;

/**
@api {put} /api/user Update user details
@apiName Update user
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiParam {String} full_name User's full name
@apiParam {String} dp File _id that is received from File upload api
@apiParam {Array} music User's music choice
@apiParam {String} relationship User's relationship status
@apiParam {String} city User's city
@apiParam {String} about User's bio
@apiParam {Date} dob User's date of birth [Format YYYY-MM-DD]
@apiParamExample {json} Input
{
  "full_name": "Smit Ray",
  "dp": "File _id",
  "music": [
    "trance",
    "pop",
    "rock"
  ],
  "relationship": "single",
  "city": "london",
  "about": "something about me",
  "dob": "1987-02-16"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "user": [Object]
  },
  "message": "user updated successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 422 Unprocessable entity
 */

export const userUpdate = async (ctx) => {
  try {
    user = await userCrud.updateUser({
      params: {
        qr: {
          _id: ctx.state.user.auth
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        user
      },
      message: 'user updated successfully'
    };
  }
};

/**
@api {get} /api/user/friends Get friends list
@apiName Get friends list
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "users": [Object]
  },
  "message": "all friends fetche"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

export const getFriends = async (ctx) => {
  try {
    const frreq = await authCrud.single({
      qr: {
        _id: ctx.state.user.auth
      },
      select: 'user username',
      populate: [{
        path: 'user',
        model: 'userModel',
        select: 'permission',
        populate: [{
          path: 'permission',
          model: 'permissionModel',
          select: 'friends',
          populate: [{
            path: 'friends',
            model: 'authModel',
            select: 'user username',
            populate: [{
              path: 'user',
              model: 'userModel',
              select: 'full_name dp music relationship city about',
              populate: [{
                path: 'dp',
                model: 'filesModel'
              }]
            }]
          }]
        }]
      }]
    });

    ctx.body = {
      success: 1,
      data: frreq,
      message: 'all friends fetched'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/user/friends Delete friend
@apiName Remove friend
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {ObjectId} fid friend's ID
@apiParamExample {json} Input
{
  "fid": [ObjectID]
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

export const deleteFriends = async (ctx) => {
  try {
    const frreq = await userCrud.deleteFriend({
      fid: ctx.request.body.fid,
      auth: ctx.state.user.auth
    });
    ctx.body = {
      success: 1,
      data: {
        user: frreq
      },
      message: 'friend deleted'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/user/request/accept Accept request
@apiName Accept friend request
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {ObjectId} fid friend's ID
@apiParamExample {json} Input
{
  "fid": [ObjectID]
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

export const acceptRequest = async (ctx) => {
  try {
    const frreq = await userCrud.acceptRequest({
      fid: ctx.request.body.fid,
      auth: ctx.state.user.auth
    });
    ctx.body = {
      success: 1,
      data: {
        user: frreq
      },
      message: 'request accepted'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/user/request/reject Remove request
@apiName Remove friend request
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {ObjectId} fid friend's ID
@apiParamExample {json} Input
{
  "fid": [ObjectID]
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

export const rejectRequest = async (ctx) => {
  try {
    const frreq = await userCrud.rejectRequest({
      fid: ctx.request.body.fid,
      auth: ctx.state.user.auth
    });
    ctx.body = {
      success: 1,
      data: {
        user: frreq
      },
      message: 'request rejected'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {get} /api/user/request Get friends request
@apiName Get friends request list
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "users": [Object]
  },
  "message": "all requests fetche"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

export const getRequests = async (ctx) => {
  try {
    const frreq = await authCrud.single({
      qr: {
        _id: ctx.state.user.auth
      },
      select: 'user username',
      populate: [{
        path: 'user',
        model: 'userModel',
        select: 'permission',
        populate: [{
          path: 'permission',
          model: 'permissionModel',
          select: 'request',
          populate: [{
            path: 'request',
            model: 'authModel',
            select: 'user username',
            populate: [{
              path: 'user',
              model: 'userModel',
              select: 'full_name dp music relationship city about',
              populate: [{
                path: 'dp',
                model: 'filesModel'
              }]
            }]
          }]
        }]
      }]
    });

    ctx.body = {
      success: 1,
      data: frreq,
      message: 'all request fetched'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {post} /api/user/request Create request
@apiName Create friend request
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {ObjectId} fid friend's ID
@apiParamExample {json} Input
{
  "fid": [ObjectID]
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

export const createRequest = async (ctx) => {
  try {
    const frreq = await userCrud.createRequest({
      fid: ctx.request.body.fid,
      auth: ctx.state.user.auth
    });
    ctx.body = {
      success: 1,
      data: {
        user: frreq
      },
      message: 'request sent'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};
