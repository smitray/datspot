import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import { Crud } from '@utl';

const addressSchema = new mongoose.Schema({
  // street: String,
  // number: String,
  // zip: String,
  city: String
  // country: String
});

addressSchema.plugin(uniqueValidator);
addressSchema.plugin(timestamp);

const addressModel = mongoose.model('addressModel', addressSchema);
const addressCrud = new Crud(addressModel);

export {
  addressModel,
  addressCrud
};
