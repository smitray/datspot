// import { userCrud, permissionCrud } from './';

function userStream(socket, app) {
  socket.on('user-join', ({ uid }) => {
    socket.join(uid);
    app.io.to(uid).emit('conf-message', {
      message: 'User created on socket',
      uid
    });
  });
}

export default userStream;
