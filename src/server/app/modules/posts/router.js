// import { isAuthenticated } from '@mid';

import {
  postsAll,
  postsSingle,
  postsCreate,
  postsUpdate,
  postsDelete
} from './';

export const baseUrl = '/api/posts';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      postsAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      postsSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      postsUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      postsDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      postsCreate
    ]
  }
];
