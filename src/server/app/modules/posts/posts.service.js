import { Crud } from '@utl';

import postsModel from './posts.model';

class Serviceposts extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }

  async createPost(options) {
    const {
      title,
      content,
      gallery,
      from,
      tagged,
      postType,
      location,
      featured
    } = options;

    
  }
}

const postsCrud = new Serviceposts(postsModel);
export default postsCrud;
