export * as postsRouteProps from './router';
export {
  postsAll,
  postsSingle,
  postsCreate,
  postsUpdate,
  postsDelete
} from './controller';
export { default as postsModel } from './posts.model';
export { default as postsCrud } from './posts.service';

