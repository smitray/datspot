import { postsCrud } from './';


let postss;
let posts;
let postsNew;

/**
@api {get} /api/posts Get all posts
@apiName Get all posts
@apiGroup Posts
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    postss: [Object]
  },
  message: posts details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const postsAll = async (ctx) => {
  try {
    postss = await postsCrud.get({
      qr: {
        sharedTo: ctx.state.user.auth
      },
      populate: [{
        path: 'gallery',
        model: 'galleryModel',
        populate: [{
          path: 'galleryItems',
          model: 'galleryItemsModel',
          options: {
            limit: 6
          },
          populate: [{
            path: 'image',
            model: 'filesModel'
          }]
        }]
      }, {
        path: 'from',
        model: 'authModel',
        select: 'acc_type user',
        populate: [{
          path: 'user',
          model: 'userModel',
          select: 'full_name dp',
          populate: [{
            path: 'dp',
            model: 'filesModel'
          }]
        }]
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        postss
      },
      message: 'postss fetched successfully'
    };
  }
};


/**
@api {get} /api/posts/:id Get single posts
@apiName Get single posts
@apiGroup Posts
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    posts: [Object]
  },
  message: posts details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const postsSingle = async (ctx) => {
  try {
    posts = await postsCrud.single({
      qr: {
        _id: ctx.params.id
      },
      populate: [{
        path: 'gallery',
        model: 'galleryModel',
        populate: [{
          path: 'galleryItems',
          model: 'galleryItemsModel',
          options: {
            limit: 6
          },
          populate: [{
            path: 'image',
            model: 'filesModel'
          }]
        }]
      }, {
        path: 'from',
        model: 'authModel',
        select: 'acc_type user',
        populate: [{
          path: 'user',
          model: 'userModel',
          select: 'full_name dp',
          populate: [{
            path: 'dp',
            model: 'filesModel'
          }]
        }]
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        posts
      },
      message: 'posts fetched successfully'
    };
  }
};

/**
@api {post} /api/posts Create posts
@apiName Create posts
@apiGroup Posts
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    posts: [Object]
  },
  message: posts created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const postsCreate = async (ctx) => {
  try {
    postsNew = await postsCrud.createPost({
      ...ctx.request.body,
      from: ctx.state.user.auth
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        posts: postsNew
      },
      message: 'posts created successfully'
    };
  }
};

/**
@api {put} /api/posts/:id Update posts
@apiName Update posts
@apiGroup Posts
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    posts: [Object]
  },
  message: posts updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const postsUpdate = async (ctx) => {
  try {
    posts = await postsCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        posts
      },
      message: 'posts updated successfully'
    };
  }
};

/**
@api {delete} /api/posts/:id Delete posts
@apiName Delete posts
@apiGroup Posts
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    posts: [Object]
  },
  message: posts deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const postsDelete = async (ctx) => {
  try {
    posts = await postsCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        posts
      },
      message: 'posts deleted successfully'
    };
  }
};

export {
  postsAll,
  postsSingle,
  postsCreate,
  postsUpdate,
  postsDelete
};
