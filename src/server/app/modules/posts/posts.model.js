import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const postsSchema = new mongoose.Schema({
  title: String,
  content: String,
  gallery: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'galleryModel',
    default: null
  },
  from: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'authModel'
  },
  tagged: String,
  postType: {
    type: String,
    default: 'personal'
  },
  sharedTo: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'authModel'
  }],
  location: String,
  featured: Boolean
});

postsSchema.plugin(uniqueValidator);
postsSchema.plugin(timestamp);

const postsModel = mongoose.model('postsModel', postsSchema);

export default postsModel;
