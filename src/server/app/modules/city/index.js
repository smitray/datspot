export * as cityRouteProps from './router';
export {
  cityAll,
  citySingle,
  cityCreate,
  cityUpdate,
  cityDelete
} from './controller';
export { default as cityModel } from './city.model';
export { default as cityCrud } from './city.service';

