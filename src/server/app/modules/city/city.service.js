import { Crud } from '@utl';

import cityModel from './city.model';

class Servicecity extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const cityCrud = new Servicecity(cityModel);
export default cityCrud;
