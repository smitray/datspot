import { cityCrud } from './';


let citys;
let city;
let cityNew;

/**
@api {get} /api/city Get all citys
@apiName Get citys
@apiGroup City
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "citys": [Object]
  },
  "message": "citys fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

const cityAll = async (ctx) => {
  try {
    citys = await cityCrud.get({
      populate: [{
        path: 'image',
        model: 'filesModel'
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        citys
      },
      message: 'citys fetched successfully'
    };
  }
};

const citySingle = async (ctx) => {
  try {
    city = await cityCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        city
      },
      message: 'city fetched successfully'
    };
  }
};

const cityCreate = async (ctx) => {
  try {
    cityNew = await cityCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        city: cityNew
      },
      message: 'city created successfully'
    };
  }
};

const cityUpdate = async (ctx) => {
  try {
    city = await cityCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        city
      },
      message: 'city updated successfully'
    };
  }
};

const cityDelete = async (ctx) => {
  try {
    city = await cityCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        city
      },
      message: 'city deleted successfully'
    };
  }
};

export {
  cityAll,
  citySingle,
  cityCreate,
  cityUpdate,
  cityDelete
};
