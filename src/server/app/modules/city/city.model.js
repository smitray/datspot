import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const citySchema = new mongoose.Schema({
  cityName: String,
  image: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'filesModel',
    default: null
  },
  cityValue: String
});

citySchema.plugin(uniqueValidator);
citySchema.plugin(timestamp);

const cityModel = mongoose.model('cityModel', citySchema);

export default cityModel;
