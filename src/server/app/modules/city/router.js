// import { isAuthenticated } from '@mid';

import {
  cityAll,
  citySingle,
  cityCreate,
  cityUpdate,
  cityDelete
} from './';

export const baseUrl = '/api/city';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      cityAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      citySingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      cityUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      cityDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      cityCreate
    ]
  }
];
