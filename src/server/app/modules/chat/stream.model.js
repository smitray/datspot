import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import { Crud } from '@utl';

const streamSchema = new mongoose.Schema({
  st_type: {
    type: String,
    default: 'chat'
  },
  content: {
    type: String,
    default: null
  },
  file: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'filesModel',
    default: null
  },
  from: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'authModel',
    default: null
  },
  to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'authModel',
    default: null
  },
  post_time: {
    type: Date,
    default: null
  },
  read: {
    type: Boolean,
    default: false
  },
  sent: {
    type: Boolean,
    default: true
  },
  received: {
    type: Boolean,
    default: false
  }
});

streamSchema.plugin(uniqueValidator);
streamSchema.plugin(timestamp);

const streamModel = mongoose.model('streamModel', streamSchema);
const streamCrud = new Crud(streamModel);

export {
  streamModel,
  streamCrud
};
