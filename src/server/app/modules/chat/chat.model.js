import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const chatSchema = new mongoose.Schema({
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'authModel'
  }],
  streams: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'streamModel',
    default: null
  }]
});

chatSchema.plugin(uniqueValidator);
chatSchema.plugin(timestamp);

const chatModel = mongoose.model('chatModel', chatSchema);

export default chatModel;
