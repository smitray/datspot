import { isAuthenticated } from '@mid';

import {
  chatAll,
  chatSingle
} from './';

export const baseUrl = '/api/chat';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      isAuthenticated,
      chatAll
    ]
  },
  {
    method: 'GET',
    route: '/:id/:page?/:size?',
    handlers: [
      isAuthenticated,
      chatSingle
    ]
  }
];
