// import _ from 'lodash';

import { chatCrud, streamCrud } from './';
import { authCrud } from '../auth';
import { filesCrud } from '../files';

async function getUserDet(uid) {
  try {
    const user = await authCrud.single({
      qr: {
        _id: uid
      },
      select: 'user',
      populate: [{
        path: 'user',
        model: 'userModel',
        select: 'full_name dp music relationship city',
        populate: [{
          path: 'dp',
          model: 'filesModel'
        }]
      }]
    });
    return user;
  } catch (e) {
    throw new Error(e.message);
  }
}

async function getFile(file) {
  try {
    const fileDet = await filesCrud.single({
      qr: {
        _id: file
      }
    });
    return fileDet;
  } catch (e) {
    throw new Error(e.message);
  }
}

export default (socket, app) => {
  socket.on('chat-request', async (data) => {
    try {
      const {
        result,
        stream
      } = await chatCrud.createChat(data);
      const {
        stType,
        content,
        file,
        from,
        to,
        postTime
      } = data;

      let fileDet = null;
      if (stType === 'file') {
        fileDet = await getFile(file);
      }
      const fromUser = await getUserDet(from);
      const toUser = await getUserDet(to);
      app.io.to(from).emit('chat-request', {
        stType,
        content,
        file: fileDet,
        postTime,
        from: fromUser,
        to: toUser,
        chatId: result._id,
        streamId: stream._id
      });
      app.io.to(to).emit('chat-request', {
        stType,
        content,
        file: fileDet,
        postTime,
        from: fromUser,
        to: toUser,
        chatId: result._id,
        streamId: stream._id
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('room-message', async (data) => {
    try {
      const {
        chatId,
        from,
        to,
        stType,
        content,
        file,
        postTime
      } = data;
      const stream = await chatCrud.addChat(data);

      let fileDet = null;

      if (stType === 'file') {
        fileDet = await getFile(file);
      }

      const fromUser = await getUserDet(from);
      const toUser = await getUserDet(to);

      app.io.to(from).emit('room-message', {
        stType,
        content,
        file: fileDet,
        postTime,
        from: fromUser,
        to: toUser,
        chatId,
        streamId: stream._id
      });
      app.io.to(to).emit('room-message', {
        stType,
        content,
        file: fileDet,
        postTime,
        from: fromUser,
        to: toUser,
        chatId,
        streamId: stream._id
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('message-received', async ({ streamId, from, chatId }) => {
    try {
      await streamCrud.put({
        params: {
          qr: {
            _id: streamId
          }
        },
        body: {
          received: true
        }
      });
      app.io.to(from).emit('message-received', {
        streamId,
        chatId
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('message-read', async ({ streamId, from, chatId }) => {
    try {
      await streamCrud.put({
        params: {
          qr: {
            _id: streamId
          }
        },
        body: {
          read: true
        }
      });
      app.io.to(from).emit('message-read', {
        streamId,
        chatId
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('delete-single-msg', async ({
    streamId,
    from,
    to,
    chatId
  }) => {
    try {
      await streamCrud.put({
        params: {
          qr: {
            _id: streamId
          }
        },
        body: {
          st_type: 'deleted'
        }
      });
      app.io.to(from).emit('delete-single-msg', {
        streamId,
        chatId
      });
      app.io.to(to).emit('delete-single-msg', {
        streamId,
        chatId
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });

  socket.on('delete-msg', async ({
    from,
    to,
    chatId
  }) => {
    try {
      await chatCrud.deleteChat(chatId);
      app.io.to(from).emit('delete-msg', {
        chatId
      });
      app.io.to(to).emit('delete-msg', {
        chatId
      });
    } catch (e) {
      throw new Error(e.message);
    }
  });
};
