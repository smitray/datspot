import { chatCrud } from './';


let chats;
let chat;

/**
@api {get} /api/chat Get all chat
@apiName Get all chat
@apiGroup Chat
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    chats: [Object]
  },
  message: chat details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const chatAll = async (ctx) => {
  try {
    chats = await chatCrud.get({
      qr: {
        users: ctx.state.user.auth
      },
      populate: [{
        path: 'users',
        model: 'authModel',
        select: 'user',
        populate: [{
          path: 'user',
          model: 'userModel',
          select: 'full_name dp music relationship city',
          populate: [{
            path: 'dp',
            model: 'filesModel'
          }]
        }]
      }, {
        path: 'streams',
        model: 'streamModel'
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        chats
      },
      message: 'chats fetched successfully'
    };
  }
};


/**
@api {get} /api/chat/:id Get single chat
@apiName Get single chat
@apiGroup Chat
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    chat: [Object]
  },
  message: chat details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const chatSingle = async (ctx) => {
  try {
    chat = await chatCrud.chatSingPaginated({
      chatId: ctx.params.id,
      page: ctx.params.page,
      size: ctx.params.size
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        chats: chat.chats,
        totalPages: chat.totalPg
      },
      message: 'chat fetched successfully'
    };
  }
};

export {
  chatAll,
  chatSingle
};
