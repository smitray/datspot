import _ from 'lodash';

import { Crud } from '@utl';

import chatModel from './chat.model';
import { streamCrud } from './stream.model';

class Servicechat extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
    this.streamCrud = streamCrud;
  }

  async createChat(options) {
    const {
      stType,
      content,
      file,
      from,
      to,
      postTime
    } = options;

    const stream = await this.streamCrud.create({
      st_type: stType,
      content,
      file,
      from,
      to,
      post_time: postTime
    });

    const chat = await this.create({});

    chat.users.push(from);
    chat.users.push(to);
    chat.streams.push(stream._id);

    return new Promise((resolve, reject) => {
      chat.save().then((result) => {
        resolve({
          result,
          stream
        });
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async addChat(options) {
    const {
      chatId,
      from,
      to,
      stType,
      content,
      file,
      postTime
    } = options;

    const stream = await this.streamCrud.create({
      st_type: stType,
      content,
      file,
      from,
      to,
      post_time: postTime
    });

    const chat = await this.single({
      qr: {
        _id: chatId
      }
    });

    chat.streams.push(stream._id);
    return new Promise((resolve, reject) => {
      chat.save().then(() => {
        resolve(stream);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async deleteChat(chatId) {
    const chat = await this.single({
      qr: {
        _id: chatId
      }
    });

    const stt = chat.streams.map(async (item) => {
      const stream = await this.streamCrud.delete({
        params: {
          qr: {
            _id: item
          }
        }
      });
      return stream._id;
    });

    await Promise.all(stt);

    return new Promise((resolve, reject) => {
      chat.remove().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async chatSingPaginated(options) {
    const {
      chatId,
      page,
      size
    } = options;

    const dataPg = page || 1;
    const dataSz = size || 20;

    const chat = await this.single({
      qr: {
        _id: chatId
      }
    });

    chat.streams.reverse();
    const offset = (dataPg - 1) * dataSz;
    const pagedItems = _.drop(chat.streams, offset).slice(0, dataSz);
    const totalPg = Math.ceil(chat.streams.length / dataSz);

    const chats = [];

    const stt = pagedItems.map(async (item) => {
      const stream = await this.streamCrud.single({
        qr: {
          _id: item
        },
        populate: [{
          path: 'file',
          model: 'filesModel'
        }, {
          path: 'from',
          model: 'authModel',
          select: 'user',
          populate: [{
            path: 'user',
            model: 'userModel',
            select: 'full_name dp music relationship city',
            populate: [{
              path: 'dp',
              model: 'filesModel'
            }]
          }]
        }, {
          path: 'to',
          model: 'authModel',
          select: 'user',
          populate: [{
            path: 'user',
            model: 'userModel',
            select: 'full_name dp music relationship city',
            populate: [{
              path: 'dp',
              model: 'filesModel'
            }]
          }]
        }]
      });
      const {
        content,
        file,
        from,
        to,
        read,
        received
      } = stream;
      chats.push({
        streamId: stream._id,
        stType: stream.st_type,
        content,
        file,
        from,
        to,
        read,
        received,
        postTime: stream.post_time
      });
    });
    await Promise.all(stt);
    return {
      chats,
      totalPg
    };
  }
}

const chatCrud = new Servicechat(chatModel);
export default chatCrud;
