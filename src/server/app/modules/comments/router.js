import { isAuthenticated } from '@mid';

import {
  commentsAll,
  commentsCreate,
  commentsUpdate,
  commentsDelete
} from './';

export const baseUrl = '/api/comments';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      isAuthenticated,
      commentsAll
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      isAuthenticated,
      commentsUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      isAuthenticated,
      commentsDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      isAuthenticated,
      commentsCreate
    ]
  }
];
