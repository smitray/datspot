import { Crud } from '@utl';

import commentsModel from './comments.model';
import { commentItemsCrud } from './commentItems.model';

class Servicecomments extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
    this.item = commentItemsCrud;
  }

  async createComment(options) {
    const {
      commentId,
      content,
      from
    } = options;

    let comment;

    if (!commentId) {
      comment = await this.create({});
    } else {
      comment = await this.single({
        qr: {
          _id: commentId
        }
      });
    }

    const comtItem = await this.item.create({
      content,
      from
    });

    comment.commentItems.push(comtItem._id);

    return new Promise((resolve, reject) => {
      comment.save().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }
}

const commentsCrud = new Servicecomments(commentsModel);
export default commentsCrud;
