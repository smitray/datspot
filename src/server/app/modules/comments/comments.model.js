import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const commentsSchema = new mongoose.Schema({
  commentItems: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'commentItemsModel'
  }]
});

commentsSchema.plugin(uniqueValidator);
commentsSchema.plugin(timestamp);

const commentsModel = mongoose.model('commentsModel', commentsSchema);

export default commentsModel;
