export * as commentsRouteProps from './router';
export {
  commentsAll,
  commentsCreate,
  commentsUpdate,
  commentsDelete
} from './controller';
export { default as commentsModel } from './comments.model';
export { default as commentsCrud } from './comments.service';
export {
  commentItemsCrud,
  commentItemsModel
} from './commentItems.model';
