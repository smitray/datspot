import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

import { Crud } from '@utl';

const commentItemsSchema = new mongoose.Schema({
  from: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'authModel'
  },
  content: String
});

commentItemsSchema.plugin(uniqueValidator);
commentItemsSchema.plugin(timestamp);

export const commentItemsModel = mongoose.model('commentItemsModel', commentItemsSchema);
export const commentItemsCrud = new Crud(commentItemsModel);
