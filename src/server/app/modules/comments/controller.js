import { commentsCrud } from './';


let commentss;
let comments;
let commentsNew;

/**
@api {get} /api/comments Get all comments
@apiName Get all comments
@apiGroup Comments
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    commentss: [Object]
  },
  message: comments details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const commentsAll = async (ctx) => {
  try {
    commentss = await commentsCrud.get({
      populate: [{
        path: 'commentItems',
        model: 'commentItemsModel',
        populate: [{
          path: 'from',
          model: 'authModel',
          select: 'acc_type user',
          populate: [{
            path: 'user',
            model: 'userModel',
            select: 'full_name dp',
            populate: [{
              path: 'dp',
              model: 'filesModel'
            }]
          }]
        }]
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        commentss
      },
      message: 'commentss fetched successfully'
    };
  }
};


/**
@api {post} /api/comments Create comments
@apiName Create comments
@apiGroup Comments
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {ObjectId} commentId comment id if exists
@apiParam {String} content comment
@apiParamExample {json} Input
{
  "commentId": [ObjectID],
  "content": "Lorem ipsum"
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    comments: [Object]
  },
  message: comments created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const commentsCreate = async (ctx) => {
  try {
    commentsNew = await commentsCrud.createComment({
      ...ctx.request.body,
      from: ctx.state.user.auth
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        comments: commentsNew
      },
      message: 'comments created successfully'
    };
  }
};

/**
@api {put} /api/comments/:id Update comments
@apiName Update comments
@apiGroup Comments
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    comments: [Object]
  },
  message: comments updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const commentsUpdate = async (ctx) => {
  try {
    comments = await commentsCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        comments
      },
      message: 'comments updated successfully'
    };
  }
};

/**
@api {delete} /api/comments/:id Delete comments
@apiName Delete comments
@apiGroup Comments
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    comments: [Object]
  },
  message: comments deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const commentsDelete = async (ctx) => {
  try {
    comments = await commentsCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        comments
      },
      message: 'comments deleted successfully'
    };
  }
};

export {
  commentsAll,
  commentsCreate,
  commentsUpdate,
  commentsDelete
};
