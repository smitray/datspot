import Router from 'koa-router';

import { filesRouteProps } from './files';
import { userRouteProps, userIO } from './user';
import { authRouteProps } from './auth';
import { musicRouteProps } from './music';
import { cityRouteProps } from './city';
import { galleryRouteProps } from './gallery';
import { commentsRouteProps } from './comments';
// import { postsRouteProps } from './posts';
import { chatIO, chatRouteProps } from './chat';

const routerControllPros = [
  filesRouteProps,
  userRouteProps,
  authRouteProps,
  musicRouteProps,
  cityRouteProps,
  galleryRouteProps,
  commentsRouteProps,
  chatRouteProps
  // postsRouteProps
];

const ioControlProps = [
  userIO,
  chatIO
];

const ioControl = (app) => {
  app.io.on('connection', (socket) => {

    console.log('A user connected');

    setTimeout(() => {
      socket.emit('testerEvent', { description: 'A custom event named testerEvent!' });
    }, 4000);

    ioControlProps.forEach((ioProperty) => {
      ioProperty(socket, app);
    });

    socket.on('disconnect', () => {
      console.log('A user disconnected');
    });
  });
};

let instance;

const routerControl = (app) => {
  routerControllPros.forEach((routeProperty) => {
    instance = new Router({ prefix: routeProperty.baseUrl });
    routeProperty.routes.forEach((config) => {
      const {
        method = '',
        route = '',
        handlers = []
      } = config;

      const lastHandler = handlers.pop();

      instance[method.toLowerCase()](route, ...handlers, async (ctx) => {
        const hddd = await lastHandler(ctx);
        return hddd;
      });

      app
        .use(instance.routes())
        .use(instance.allowedMethods());
    });
  });
};

export {
  routerControl,
  ioControl
};
