import { isAuthenticated } from '@mid';

import {
  galleryAll,
  gallerySingle,
  galleryItemSingle,
  galleryCreate,
  galleryDelete,
  galleryComment,
  galleryLike,
  galleryDislike
} from './';

export const baseUrl = '/api/gallery';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      isAuthenticated,
      galleryAll
    ]
  },
  {
    method: 'PUT',
    route: '/comment/:id',
    handlers: [
      isAuthenticated,
      galleryComment
    ]
  },
  {
    method: 'PUT',
    route: '/like/:id',
    handlers: [
      isAuthenticated,
      galleryLike
    ]
  },
  {
    method: 'PUT',
    route: '/dislike/:id',
    handlers: [
      isAuthenticated,
      galleryDislike
    ]
  },
  {
    method: 'GET',
    route: '/item/:id',
    handlers: [
      isAuthenticated,
      galleryItemSingle
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      isAuthenticated,
      gallerySingle
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      isAuthenticated,
      galleryDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      isAuthenticated,
      galleryCreate
    ]
  }
];
