import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';
import { Crud } from '@utl';

const galleryItemsSchema = new mongoose.Schema({
  image: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'filesModel',
    default: null
  },
  comments: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'commentsModel',
    default: null
  },
  likes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'authModel',
    default: null
  }]
});

galleryItemsSchema.plugin(uniqueValidator);
galleryItemsSchema.plugin(timestamp);

export const galleryItemsModel = mongoose.model('galleryItemsModel', galleryItemsSchema);
export const galleryItemsCrud = new Crud(galleryItemsModel);
