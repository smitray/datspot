export * as galleryRouteProps from './router';
export {
  galleryAll,
  gallerySingle,
  galleryItemSingle,
  galleryCreate,
  galleryDelete,
  galleryComment,
  galleryLike,
  galleryDislike
} from './controller';
export { default as galleryModel } from './gallery.model';
export { default as galleryCrud } from './gallery.service';
export {
  galleryItemsModel,
  galleryItemsCrud
} from './galleryItems.model';
