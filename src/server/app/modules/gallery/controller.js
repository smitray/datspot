import { galleryCrud, galleryItemsCrud } from './';


let gallerys;
let gallery;
let galleryNew;

/**
@api {get} /api/gallery Get all gallery
@apiName Get all gallery
@apiGroup Gallery
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    gallerys: [Object]
  },
  message: gallery details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const galleryAll = async (ctx) => {
  try {
    gallerys = await galleryCrud.get({
      populate: [{
        path: 'galleryItems',
        model: 'galleryItemsModel',
        populate: [{
          path: 'image',
          model: 'filesModel'
        }]
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        gallerys
      },
      message: 'gallerys fetched successfully'
    };
  }
};


/**
@api {get} /api/gallery/:id Get single gallery
@apiName Get single gallery
@apiGroup Gallery
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    gallery: [Object]
  },
  message: gallery details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const gallerySingle = async (ctx) => {
  try {
    gallerys = await galleryCrud.single({
      qr: {
        _id: ctx.params.id
      },
      populate: [{
        path: 'galleryItems',
        model: 'galleryItemsModel',
        populate: [{
          path: 'image',
          model: 'filesModel'
        }, {
          path: 'comments',
          model: 'commentsModel',
          populate: [{
            path: 'commentItems',
            model: 'commentItemsModel',
            populate: [{
              path: 'from',
              model: 'authModel',
              select: 'acc_type user',
              populate: [{
                path: 'user',
                model: 'userModel',
                select: 'full_name dp',
                populate: [{
                  path: 'dp',
                  model: 'filesModel'
                }]
              }]
            }]
          }]
        }, {
          path: 'likes',
          model: 'authModel',
          select: 'acc_type user',
          populate: [{
            path: 'user',
            model: 'userModel',
            select: 'full_name dp',
            populate: [{
              path: 'dp',
              model: 'filesModel'
            }]
          }]
        }]
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        gallerys
      },
      message: 'gallerys fetched successfully'
    };
  }
};

/**
@api {get} /api/gallery/item/:id Get gallery single item
@apiName Get gallery single item
@apiGroup Gallery
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    gallery: [Object]
  },
  message: gallery details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const galleryItemSingle = async (ctx) => {
  try {
    gallery = await galleryItemsCrud.single({
      qr: {
        _id: ctx.params.id
      },
      populate: [{
        path: 'image',
        model: 'filesModel'
      }, {
        path: 'comments',
        model: 'commentsModel',
        populate: [{
          path: 'commentItems',
          model: 'commentItemsModel',
          populate: [{
            path: 'from',
            model: 'authModel',
            select: 'acc_type user',
            populate: [{
              path: 'user',
              model: 'userModel',
              select: 'full_name dp',
              populate: [{
                path: 'dp',
                model: 'filesModel'
              }]
            }]
          }]
        }]
      }, {
        path: 'likes',
        model: 'authModel',
        select: 'acc_type user',
        populate: [{
          path: 'user',
          model: 'userModel',
          select: 'full_name dp',
          populate: [{
            path: 'dp',
            model: 'filesModel'
          }]
        }]
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        gallery
      },
      message: 'gallery fetched successfully'
    };
  }
};

/**
@api {post} /api/gallery Create gallery
@apiName Create gallery
@apiGroup Gallery
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {ObjectId} galleryId Gallery ID if exists
@apiParam {Object} images images Object from File api
@apiParamExample {json} Input
{
  "images": [Object],
  "galleryId": [ObjectId]
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    gallery: [Object]
  },
  message: gallery created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const galleryCreate = async (ctx) => {
  try {
    galleryNew = await galleryCrud.createGallery(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        gallery: galleryNew
      },
      message: 'gallery created successfully'
    };
  }
};


/**
@api {put} /api/gallery/comment/:id Add comment to gallery
@apiName Add comment to gallery
@apiGroup Gallery
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {ObjectID} comments comment id
@apiParamExample {json} Input
{
  "comments": [Object ID]
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    gallery: [Object]
  },
  message: gallery updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

export const galleryComment = async (ctx) => {
  try {
    gallery = await galleryItemsCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
    ctx.body = {
      success: 1,
      data: {
        gallery
      },
      message: 'gallery comment added'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {put} /api/gallery/like/:id Add like to gallery
@apiName Add like to gallery
@apiGroup Gallery
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    gallery: [Object]
  },
  message: gallery updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

export const galleryLike = async (ctx) => {
  try {
    gallery = await galleryCrud.addLike({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: {
        auth: ctx.state.user.auth
      }
    });
    ctx.body = {
      success: 1,
      data: {
        gallery
      },
      message: 'gallery like added'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

/**
@api {put} /api/gallery/dislike/:id Remove like to gallery
@apiName Remove like to gallery
@apiGroup Gallery
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    gallery: [Object]
  },
  message: gallery updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

export const galleryDislike = async (ctx) => {
  try {
    gallery = await galleryCrud.removeLike({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: {
        auth: ctx.state.user.auth
      }
    });
    ctx.body = {
      success: 1,
      data: {
        gallery
      },
      message: 'gallery like added'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};


/**
@api {delete} /api/gallery/:id Delete gallery
@apiName Delete gallery
@apiGroup Gallery
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    gallery: [Object]
  },
  message: gallery deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const galleryDelete = async (ctx) => {
  try {
    gallery = await galleryCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        gallery
      },
      message: 'gallery deleted successfully'
    };
  }
};

export {
  galleryAll,
  gallerySingle,
  galleryItemSingle,
  galleryCreate,
  galleryDelete
};
