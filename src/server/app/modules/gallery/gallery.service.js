import { Crud } from '@utl';

import galleryModel from './gallery.model';
import { galleryItemsCrud } from './galleryItems.model';

class Servicegallery extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
    this.item = galleryItemsCrud;
  }

  async createGallery(options) {
    const {
      galleryId,
      images
    } = options;

    let gallery;

    if (!galleryId) {
      gallery = await this.create({});
    } else {
      gallery = await this.single({
        qr: {
          _id: galleryId
        }
      });
    }

    const itt = images.map(async (img) => {
      const items = await this.item.create({
        image: img._id
      });
      return items._id;
    });

    const galItems = await Promise.all(itt);

    galItems.forEach((glit) => {
      gallery.galleryItems.push(glit);
    });

    return new Promise((resolve, reject) => {
      gallery.save().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async addLike(options) {
    const item = await this.item.single({
      qr: options.params.qr
    });

    item.likes.push(options.body.auth);

    return new Promise((resolve, reject) => {
      item.save().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  async removeLike(options) {
    const item = await this.item.single({
      qr: options.params.qr
    });

    item.likes.remove(options.body.auth);

    return new Promise((resolve, reject) => {
      item.save().then((result) => {
        resolve(result);
      }).catch((e) => {
        reject(e);
      });
    });
  }
}

const galleryCrud = new Servicegallery(galleryModel);
export default galleryCrud;
