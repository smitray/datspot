import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const gallerySchema = new mongoose.Schema({
  galleryItems: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'galleryItemsModel',
    default: null
  }]
});

gallerySchema.plugin(uniqueValidator);
gallerySchema.plugin(timestamp);

const galleryModel = mongoose.model('galleryModel', gallerySchema);

export default galleryModel;
