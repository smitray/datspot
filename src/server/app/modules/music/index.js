export * as musicRouteProps from './router';
export {
  musicAll,
  musicSingle,
  musicCreate,
  musicUpdate,
  musicDelete
} from './controller';
export { default as musicModel } from './music.model';
export { default as musicCrud } from './music.service';

