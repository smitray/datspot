import { Crud } from '@utl';

import musicModel from './music.model';

class Servicemusic extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const musicCrud = new Servicemusic(musicModel);
export default musicCrud;
