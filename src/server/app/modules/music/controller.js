import { musicCrud } from './';


let musics;
let music;
let musicNew;

/**
@api {get} /api/music Get all musics
@apiName Get musics
@apiGroup Music
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "musics": [Object]
  },
  "message": "musics fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

const musicAll = async (ctx) => {
  try {
    musics = await musicCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        musics
      },
      message: 'musics fetched successfully'
    };
  }
};

/**
@api {get} /api/music/:id Get single music
@apiName Get music
@apiGroup Music
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "music": [Object]
  },
  "message": "music fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
 */

const musicSingle = async (ctx) => {
  try {
    music = await musicCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        music
      },
      message: 'music fetched successfully'
    };
  }
};

/**
@api {post} /api/music Create music record
@apiName Create music
@apiGroup Music
@apiParam {String} genre
@apiParam {String} genValue
@apiParamExample {json} Input
{
  "genre": "Jazz",
  "genValue": "jazz"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "music": [Object]
  },
  "message": "music created successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const musicCreate = async (ctx) => {
  try {
    musicNew = await musicCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        music: musicNew
      },
      message: 'music created successfully'
    };
  }
};


const musicUpdate = async (ctx) => {
  try {
    music = await musicCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        music
      },
      message: 'music updated successfully'
    };
  }
};

const musicDelete = async (ctx) => {
  try {
    music = await musicCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        music
      },
      message: 'music deleted successfully'
    };
  }
};

export {
  musicAll,
  musicSingle,
  musicCreate,
  musicUpdate,
  musicDelete
};
