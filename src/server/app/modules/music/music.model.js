import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const musicSchema = new mongoose.Schema({
  genre: String,
  genValue: String
});

musicSchema.plugin(uniqueValidator);
musicSchema.plugin(timestamp);

const musicModel = mongoose.model('musicModel', musicSchema);

export default musicModel;
