// import { isAuthenticated } from '@mid';

import {
  musicAll,
  musicSingle,
  musicCreate,
  musicUpdate,
  musicDelete
} from './';

export const baseUrl = '/api/music';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      musicAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      musicSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      musicUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      musicDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      musicCreate
    ]
  }
];
