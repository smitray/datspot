import Vue from "vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCheckDouble,
  faPaperPlane,
  faArrowLeft,
  faHeart,
  faComments,
  faDotCircle,
  faUserCheck,
  faTimes,
  faCocktail,
  faPen,
  faBars,
  faEnvelope
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
  faEnvelope,
  faBars,
  faPen,
  faCocktail,
  faCheckDouble,
  faPaperPlane,
  faArrowLeft,
  faHeart,
  faComments,
  faDotCircle,
  faUserCheck,
  faTimes
);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.config.productionTip = false;
