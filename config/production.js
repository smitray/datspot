module.exports = {
  server: {
    port: 3000,
    compress: false
  },
  db: {
    host: "localhost",
    dbName: "datspot",
    debug: false,
    options: {
      userName: false,
      passWord: false,
      port: 27017
    }
  },
  secret: ["PTcmnrMxCF", "hvV9BnXgwm"],
  grant: {
    server: {
      host: "codends.net"
    }
  },
  baseUrl: "http://localhost:3000"
};
