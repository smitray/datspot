const isDev = require("isdev");
const cfg = require("config");
const path = require("path");

module.exports = {
  srcDir: cfg.get("paths.app.client"),
  buildDir: cfg.get("paths.dist.client"),
  rootDir: "./",
  head: {
    title: "Datspot - Bringing people and places together",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Datspot - Bringing people and places together"
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  css: ["normalize.css", "@/assets/css/main.css"],
  loading: { color: "#ee3e83" },
  build: {
    vendor: ["vue-js-modal"],
    extractCSS: {
      allChunks: true
    },
    postcss: {
      plugins: {
        "postcss-partial-import": {},
        "postcss-crip": {},
        "postcss-nested-props": {},
        "postcss-map": {
          basePath: path.resolve(__dirname, "styleVars"),
          maps: ["fonts.yml", "colors.yml", "breakpoints.yml"]
        },
        "postcss-mixins": {},
        "postcss-advanced-variables": {},
        "postcss-short": {},
        "postcss-cssnext": {
          browsers: [
            "last 5 versions",
            "Opera 12.1",
            "safari >= 8",
            "ie >= 10",
            "ff >= 20",
            "ios 6",
            "android 4",
            "ie >= 9"
          ],
          features: {
            nesting: false
          }
        },
        "postcss-nested": {},
        "postcss-ref": {},
        "postcss-property-lookup": {},
        "postcss-utilities": {},
        "rucksack-css": {},
        "postcss-extend": {},
        "postcss-media-minmax": {},
        "postcss-merge-rules": {},
        "css-mqpacker": {}
      }
    }
  },
  modules: ["@nuxtjs/pwa", "@nuxtjs/workbox", "@nuxtjs/axios"],
  axios: {
    baseURL: cfg.get("baseUrl")
  },
  manifest: {
    name: "Datspot",
    description: "Datspot - Bringing people and places together",
    theme_color: "#000"
  },
  plugins: [
    { src: "~/plugins/vue-awesome.js", ssr: false },
    { src: "~/plugins/vue-fontawesome.js", ssr: false },
    { src: "~/plugins/vue-chat-scroll.js", ssr: false },
    { src: "~/plugins/vue-dragscroll.js", ssr: false },
    { src: "~/plugins/modals.js", ssr: false }
  ]
};
